package com.projet.projet.services;

import java.util.List;

import com.projet.projet.entities.Etudiant;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;


public interface IserviceEtudiant {
	 public List<Etudiant> getAllEtudiant() ;
	  void addEtudiant(Etudiant etudinat,MultipartFile mf) throws IOException;

	   public Etudiant updateEtudiant(Etudiant etudinat,Integer id) ;
	 
	    void deleteEtudiant(Integer idEtudiant) throws IOException;
	    String saveImage(MultipartFile mf) throws IOException;
	    void supprimerImage(Integer idEtudiant) throws IOException;
	    Etudiant retrieveEtudiant(Integer idEtudiant);
	    //List<Etudiant> retriveEtudiantByDepartement (String nomDepart);
	    public byte[] getimage(int id)throws IOException ;
	    public String generecarteetudpdf(Integer idEtudiant);
}
