package com.projet.projet.services;

import java.util.List;

import org.springframework.stereotype.Service;


import com.projet.projet.dao.DepartementRepo;
import com.projet.projet.entities.Departement;
import com.projet.projet.entities.Etudiant;
@Service


public class DepartementServicelmp implements IServiceDepartement{
	public DepartementServicelmp(DepartementRepo departementRepository) {
		super();
		this.departementRepository = departementRepository;
	}
	DepartementRepo departementRepository;
	
    
    @Override
    public List<Departement> retrieveAllDepartement() {
        return departementRepository.findAll();
    }

    @Override
    public Departement addDepartement(Departement departement) {
        return departementRepository.save(departement);
    }

    @Override
    public Departement updateDepartement(Departement D,Long id) {
    	Departement dp = departementRepository.findById(id).get();
    	dp=D ;
    	return departementRepository.save(dp);
    }

    @Override
    public void deleteDepartement(Long id) {
        departementRepository.deleteById(id);

    }
    @Override
    public Departement getDepartement(Long id) {
        return departementRepository.findById(id).get();
    }

	
	@Override
	public List<Etudiant> getetudiantpardepartement(Long id) {
		return departementRepository.getById(id).getEtudiants();
	}

	
}
