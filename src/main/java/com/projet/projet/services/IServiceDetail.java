package com.projet.projet.services;

import java.util.List;

import com.projet.projet.entities.DetailUniversite;



public interface IServiceDetail {
	List<DetailUniversite> retrieveAlldetail();
	   

    DetailUniversite addDetail(DetailUniversite detail);

    DetailUniversite updateDetail(DetailUniversite D);


    void deleteDetail(Long id);
}
