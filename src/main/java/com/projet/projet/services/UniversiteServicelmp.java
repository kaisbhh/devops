package com.projet.projet.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.projet.projet.entities.Departement;
import com.projet.projet.entities.Universite;
import com.projet.projet.dao.UniversiteRepo;
@Service


public class UniversiteServicelmp implements IServiceUniversite {
	public UniversiteServicelmp(UniversiteRepo universiteRepo) {
		super();
		this.universiteRepo = universiteRepo;
	}
	UniversiteRepo universiteRepo;
	@Override
    public List<Universite> getAllUniversite() {
        return universiteRepo.findAll();
    }

    @Override
    public Universite addUniversite(Universite U,MultipartFile mf) throws IOException{
    	String photo;
    	if(!mf.getOriginalFilename().equals("")) {
    		photo= saveImage(mf);
    		U.setPhoto(photo);
    	}
        return universiteRepo.save(U) ;
    }

    @Override
    public Universite updateUniversite(Universite U,Long id) {
    	Universite u = universiteRepo.findById(id).get();
    	u=U ;
        return universiteRepo.save(u);
    }

    @Override
    public void deleteUniversite(Long id) throws IOException{
    	if(!universiteRepo.getById(id).getPhoto().equals("")) {
    		supprimerImage(id);
    	}
        universiteRepo.deleteById(id);

    }
    @Override
    public Universite getUniversite(Long id) {
        return universiteRepo.findById(id).get();
    }
	@Override
	public String saveImage(MultipartFile mf) throws IOException {
		String nameFile = mf.getOriginalFilename();
		String tab[] =nameFile.split("\\.");
		String fileModif = tab[0]+"_"+System.currentTimeMillis()+"."+tab[1];
		
		String chemin = System.getProperty("user.dir")+"/src/main/webapp/images/";
		Path p = Paths.get(chemin,fileModif);
		Files.write(p,mf.getBytes());	
		System.out.println(chemin);
		return fileModif ;
	}
	@Override
	public void supprimerImage(Long id) throws IOException {
		Universite universite = universiteRepo.getById(id);
		
		String chemin = System.getProperty("user.dir")+ "/src/main/webapp/images";
		Path p = Paths.get(chemin,universite.getPhoto());
		System.out.println(p);
		Files.delete(p);
		
	}

	@Override
	public List<Departement> getDepartementbyuniversite(Long id) {
		
			return universiteRepo.getById(id).getDepartments();
		}

	@Override
	public List<Universite> getUniversiteByName(String name) {
		
		return  universiteRepo.findUniversiteByNomUniversite(name);
	}
	public byte[] getimage(Long id) throws IOException {
    	String nomImage=universiteRepo.findById(id).get().getPhoto();
    	Path p=Paths.get(System.getProperty("user.dir")+"/src/main/webapp/images/",nomImage);
    	
    	return Files.readAllBytes(p);
    }

	@Override
	public Universite addUni(Universite U) throws IOException {
		// TODO Auto-generated method stub
		return universiteRepo.save(U) ;
	}
	
}
