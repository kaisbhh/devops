package com.projet.projet.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;


import com.projet.projet.entities.Departement;
import com.projet.projet.entities.Universite;



public interface IServiceUniversite {
	  List<Universite> getAllUniversite();
	    Universite addUniversite(Universite U,MultipartFile mf) throws IOException;
	    Universite addUni(Universite U) throws IOException;
	    Universite updateUniversite(Universite U,Long id);
	    void deleteUniversite(Long id) throws IOException;
	    String saveImage(MultipartFile mf) throws IOException;
	    void supprimerImage(Long id) throws IOException;
	    Universite getUniversite(Long id);
	    public List<Departement> getDepartementbyuniversite(Long idc);
	    List<Universite>  getUniversiteByName(String name);
	    public byte[] getimage(Long id)throws IOException ;
}
