package com.projet.projet.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;


import com.projet.projet.entities.Professeur;

public interface IServiceProfesseur {
	public List<Professeur> getAllProfesseur() ;
	  void addProfesseur(Professeur p,MultipartFile mf) throws IOException;

	   public Professeur updateProfesseur(Professeur professeur,Integer id) ;
	 
	    void deleteProfesseur(Integer idProf) throws IOException;
	    String saveImage(MultipartFile mf) throws IOException;
	    void supprimerImage(Integer idProf) throws IOException;
	    Professeur retrieveProfesseur(Integer idProf);
	    void assignprofesseurtodepartement(Integer idp, Long idDep);
	    public byte[] getimage(int id)throws IOException ;
}
