package com.projet.projet.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.projet.projet.dao.ChefRepo;
import com.projet.projet.entities.ChefDepart;


@Service
public class ChefServicelmp implements IServiceChef {

	private ChefRepo chefrepo ;
	public ChefServicelmp(ChefRepo chefrepo) {
		super();
		this.chefrepo = chefrepo;
	}

	@Override
	public List<ChefDepart> getAllChefDepart() {
		return chefrepo.findAll();
	}

	@Override
	public void addChefDepart(ChefDepart c, MultipartFile mf) throws IOException {
		// TODO Auto-generated method stub
		String photo;
    	if(!mf.getOriginalFilename().equals("")) {
    		photo= saveImage(mf);
    		c.setPhoto(photo);
    	}
    	chefrepo.save(c);
		
	}

	@Override
	public ChefDepart updateChefDepart(ChefDepart chef) {
		// TODO Auto-generated method stub
		return chefrepo.save(chef);
	}

	@Override
	public void deleteChefDepart(Integer idC) throws IOException {
		if(!chefrepo.getById(idC).getPhoto().equals("")) {
    		supprimerImage(idC);
    	}
		chefrepo.deleteById(idC);
		
	}

	@Override
	public String saveImage(MultipartFile mf) throws IOException {
		String nameFile = mf.getOriginalFilename();
		String tab[] =nameFile.split("\\.");
		String fileModif = tab[0]+"_"+System.currentTimeMillis()+"."+tab[1];
		
		String chemin = System.getProperty("user.dir")+"/src/main/webapp/images/";
		Path p = Paths.get(chemin,fileModif);
		Files.write(p,mf.getBytes());	
		System.out.println(chemin);
		return fileModif ;
	}

	@Override
	public void supprimerImage(Integer idc) throws IOException {
		// TODO Auto-generated method stub
ChefDepart chef = chefrepo.getById(idc);
		
		String chemin = System.getProperty("user.dir")+ "/src/main/webapp/images/";
		Path p = Paths.get(chemin,chef.getPhoto());
		System.out.println(p);
		Files.delete(p);
	}

	@Override
	public ChefDepart retrieveChefDepart(Integer idC) {
		// TODO Auto-generated method stub
		ChefDepart chef = chefrepo.findById(idC).orElse(null);
        return  chef;
	}

	@Override
	public byte[] getimage(int id) throws IOException {
		// TODO Auto-generated method stub
		String nomImage=chefrepo.findById(id).get().getPhoto();
    	Path p=Paths.get(System.getProperty("user.dir")+"/src/main/webapp/images/",nomImage);
    	
    	return Files.readAllBytes(p);
	}

}
