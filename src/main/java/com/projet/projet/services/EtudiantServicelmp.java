package com.projet.projet.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import com.projet.projet.dao.EtudiantRepo;
import com.projet.projet.entities.Departement;
import com.projet.projet.entities.Etudiant;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;


@Service




public class EtudiantServicelmp implements IserviceEtudiant {
	
	
	public EtudiantServicelmp(EtudiantRepo etudinatRepository) {
		super();
		this.etudinatRepository = etudinatRepository;
	}


	private   EtudiantRepo etudinatRepository ;
	
    @Override
    public List<Etudiant> getAllEtudiant() {
        return (List<Etudiant>) etudinatRepository.findAll();
    }

    @Override
    public void addEtudiant(Etudiant etudiant,MultipartFile mf) throws IOException {
    	String photo;
    	if(!mf.getOriginalFilename().equals("")) {
    		photo= saveImage(mf);
    		etudiant.setPhoto(photo);
    	}
         etudinatRepository.save(etudiant);
    }

    @Override
    public Etudiant updateEtudiant(Etudiant etudiant,Integer id) {
    	Etudiant e = etudinatRepository.findById(id).get();
    	e=etudiant ;
    	return etudinatRepository.save(e);
    }

    @Override
    public void deleteEtudiant(Integer idEtudiant) throws IOException { 
    	if(!etudinatRepository.getById(idEtudiant).getPhoto().equals("")) {
    		supprimerImage(idEtudiant);
    	}
    etudinatRepository.deleteById(idEtudiant);
    }

   
   // @Override
    //public List<Etudiant> retriveEtudiantByClass(String nomClasse) {
      //  return etudinatRepository.retriveEtudiantByClasse(nomClasse);
    //}
    
    @Override
    public Etudiant retrieveEtudiant(Integer idEtudiant) {
        Etudiant e = etudinatRepository.findById(idEtudiant).orElse(null);
        return  e;
    }

	@Override
	public String saveImage(MultipartFile mf) throws IOException {
		String nameFile = mf.getOriginalFilename();
		String tab[] =nameFile.split("\\.");
		String fileModif = tab[0]+"_"+System.currentTimeMillis()+"."+tab[1];
		
		String chemin = System.getProperty("user.dir")+"/src/main/webapp/images/";
		Path p = Paths.get(chemin,fileModif);
		Files.write(p,mf.getBytes());	
		System.out.println(chemin);
		return fileModif ;
	}

	@Override
	public void supprimerImage(Integer idEtudiant) throws IOException {
Etudiant etudiant = etudinatRepository.getById(idEtudiant);
		
		String chemin = System.getProperty("user.dir")+ "/src/main/webapp/images";
		Path p = Paths.get(chemin,etudiant.getPhoto());
		System.out.println(p);
		Files.delete(p);
		
	}

	@Override
	public byte[] getimage(int id) throws IOException {
		
	    	String nomImage=etudinatRepository.findById(id).get().getPhoto();
	    	Path p=Paths.get(System.getProperty("user.dir")+"/src/main/webapp/images/",nomImage);
	    	
	    	return Files.readAllBytes(p);
	    }

	@Override
	public String generecarteetudpdf(Integer idEtudiant) {
		return null;
		
	}
	

	

	

	
}
