package com.projet.projet.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.projet.projet.dao.DetailRepo;
import com.projet.projet.entities.DetailUniversite;
@Service
public class DetailServicelmp implements IServiceDetail {

	public DetailServicelmp(DetailRepo detailRepository) {
		super();
		this.detailRepository = detailRepository;
	}
	DetailRepo detailRepository;
	@Override
	public List<DetailUniversite> retrieveAlldetail() {
		
		return detailRepository.findAll();
	}

	@Override
	public DetailUniversite addDetail(DetailUniversite detail) {
		return detailRepository.save(detail);
	}

	@Override
	public DetailUniversite updateDetail(DetailUniversite D) {
		return detailRepository.save(D);
	}

	@Override
	public void deleteDetail(Long id) {
		detailRepository.deleteById(id);
		
	}

}
