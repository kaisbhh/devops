package com.projet.projet.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.projet.projet.dao.DepartementRepo;
import com.projet.projet.dao.ProfesseurRepo;
import com.projet.projet.entities.Departement;
import com.projet.projet.entities.Professeur;



@Service
public class ProfesseurServicelmp implements IServiceProfesseur {

	
	public ProfesseurServicelmp(ProfesseurRepo profRepository, DepartementRepo departementRepository) {
		super();
		this.profRepository = profRepository;
		this.departementRepository = departementRepository;
	}
	private   ProfesseurRepo profRepository ;
	private DepartementRepo departementRepository;
	@Override
	public List<Professeur> getAllProfesseur() {
		 return (List<Professeur>) profRepository.findAll();
	}
	@Override
	public void addProfesseur(Professeur professeur, MultipartFile mf) throws IOException {
		String photo;
    	if(!mf.getOriginalFilename().equals("")) {
    		photo= saveImage(mf);
    		professeur.setPhoto(photo);
    	}
    	profRepository.save(professeur);
		
	}
	@Override
	public Professeur updateProfesseur(Professeur professeur,Integer id) {
		Professeur p = profRepository.findById(id).get() ; 
		p=professeur ; 
		return profRepository.save(p);
	}
	@Override
	public void deleteProfesseur(Integer idProf) throws IOException {
		if(!profRepository.getById(idProf).getPhoto().equals("")) {
    		supprimerImage(idProf);
    	}
		profRepository.deleteById(idProf);
		
	}
	@Override
	public String saveImage(MultipartFile mf) throws IOException {
		String nameFile = mf.getOriginalFilename();
		String tab[] =nameFile.split("\\.");
		String fileModif = tab[0]+"_"+System.currentTimeMillis()+"."+tab[1];
		
		String chemin = System.getProperty("user.dir")+"/src/main/webapp/images/";
		Path p = Paths.get(chemin,fileModif);
		Files.write(p,mf.getBytes());	
		System.out.println(chemin);
		return fileModif ;
	}
	@Override
	public void supprimerImage(Integer idProf) throws IOException {
		Professeur professeur = profRepository.getById(idProf);
		
		String chemin = System.getProperty("user.dir")+ "/src/main/webapp/images/";
		Path p = Paths.get(chemin,professeur.getPhoto());
		System.out.println(p);
		Files.delete(p);
		
	}
	@Override
	public Professeur retrieveProfesseur(Integer idProf) {
		Professeur p = profRepository.findById(idProf).orElse(null);
	        return  p;
	}
	@Override
	public void assignprofesseurtodepartement(Integer idp, Long idDep) {
		Departement departement=departementRepository.findById(idDep).orElse(null);
		Professeur p = profRepository.findById(idp).orElse(null);
        p.getDepartements().add(departement);
        profRepository.save(p);
		
	}
	@Override
	public byte[] getimage(int id) throws IOException {
		
	    	String nomImage=profRepository.findById(id).get().getPhoto();
	    	Path p=Paths.get(System.getProperty("user.dir")+"/src/main/webapp/images/",nomImage);
	    	
	    	return Files.readAllBytes(p);
	    
	}



}
