package com.projet.projet.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.projet.projet.entities.Departement;
import com.projet.projet.entities.Etudiant;
import com.projet.projet.entities.Universite;



public interface IServiceDepartement {
	  List<Departement> retrieveAllDepartement();
	   

	    Departement addDepartement(Departement departement);

	    Departement updateDepartement(Departement D,Long id);
	    Departement getDepartement(Long id) ;
	    void deleteDepartement(Long id);
	    public List<Etudiant> getetudiantpardepartement(Long id);
}
