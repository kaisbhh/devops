package com.projet.projet.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.projet.projet.entities.ChefDepart;

public interface IServiceChef {
	public List<ChefDepart> getAllChefDepart() ;
	  void addChefDepart(ChefDepart c,MultipartFile mf) throws IOException;

	   public ChefDepart updateChefDepart(ChefDepart chef) ;
	 
	    void deleteChefDepart(Integer idC) throws IOException;
	    String saveImage(MultipartFile mf) throws IOException;
	    void supprimerImage(Integer idc) throws IOException;
	    ChefDepart retrieveChefDepart(Integer idC);
	    public byte[] getimage(int id)throws IOException ;
}
