package com.projet.projet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.projet.entities.ChefDepart;


@Repository
public interface ChefRepo extends JpaRepository<ChefDepart , Integer> {

}
