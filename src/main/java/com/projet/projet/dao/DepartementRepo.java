package com.projet.projet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.projet.entities.Departement;

@Repository

public interface DepartementRepo extends JpaRepository<Departement,Long>{
	
}
