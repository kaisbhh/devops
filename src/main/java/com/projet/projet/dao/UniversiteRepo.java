package com.projet.projet.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.projet.entities.Universite;
@Repository
public interface UniversiteRepo extends JpaRepository<Universite,Long>{
	//@Query("select u from Universite u where u.nomUniversite=%:X%")
	List<Universite> findUniversiteByNomUniversite(String nomUniv);
}
