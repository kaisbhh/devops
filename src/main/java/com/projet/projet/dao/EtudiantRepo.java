package com.projet.projet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.projet.entities.Etudiant;

@Repository



public interface EtudiantRepo extends JpaRepository<Etudiant , Integer> {
	//@Query("select e from Etudiant e where e.classe.nomClasse =: nomClasse")
    //List<Etudiant> retriveEtudiantByClasse (@Param("nomClasse") String nomClasse);
}
