package com.projet.projet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.projet.entities.Professeur;

@Repository

public interface ProfesseurRepo extends JpaRepository<Professeur , Integer>{

}
