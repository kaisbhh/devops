package com.projet.projet.entities;

import java.io.Serializable;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.projet.projet.entities.Universite;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor
public class Departement implements Serializable{
	@Id @GeneratedValue (strategy = GenerationType.IDENTITY)
	 private Long idDepartement;
	    private String nomDepartement;
	    private  String code;
	    private  String description;
	    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
		@ManyToOne
	    Universite universite;
	    @JsonIgnore
	    @OneToMany(cascade = CascadeType.ALL,mappedBy = "departement")
	    private List<Etudiant> etudiants;
	    @JsonIgnore
	    @ManyToMany(mappedBy = "departements")
	    private List<Professeur> professeurs;
	    @JsonIgnore
	    @OneToOne
	    private ChefDepart chef;
		public ChefDepart getChef() {
			return chef;
		}
		public void setChef(ChefDepart chef) {
			this.chef = chef;
		}
		public Long getIdDepartement() {
			return idDepartement;
		}
		public void setIdDepartement(Long idDepartement) {
			this.idDepartement = idDepartement;
		}
		public String getNomDepartement() {
			return nomDepartement;
		}
		public void setNomDepartement(String nomDepartement) {
			this.nomDepartement = nomDepartement;
		}
		public String getCode() {
			return code;
		}
		public List<Professeur> getProfesseurs() {
			return professeurs;
		}
		public void setProfesseurs(List<Professeur> professeurs) {
			this.professeurs = professeurs;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		
		
		public Universite getUniversite() {
			return universite;
		}
		public void setUniversite(Universite universite) {
			this.universite = universite;
		}
		public List<Etudiant> getEtudiants() {
			return etudiants;
		}
		public void setEtudiants(List<Etudiant> etudiants) {
			this.etudiants = etudiants;
		}
		
	    
}
