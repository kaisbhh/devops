package com.projet.projet.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor
public class DetailUniversite implements Serializable{
	@Id @GeneratedValue (strategy = GenerationType.IDENTITY)
	 private Long idDetail;
	    private String Fondateur;
	    private  Integer Annee;
	    private  Integer nbetudiant;
	    private  Integer nbprof;
	    @OneToOne(mappedBy="detailUniversite")
	    private Universite Universite;
		public Long getIdDetail() {
			return idDetail;
		}
		public void setIdDetail(Long idDetail) {
			this.idDetail = idDetail;
		}
		public String getFondateur() {
			return Fondateur;
		}
		public void setFondateur(String fondateur) {
			Fondateur = fondateur;
		}
		public Integer getAnnee() {
			return Annee;
		}
		public void setAnnee(Integer annee) {
			Annee = annee;
		}
		public Integer getNbetudiant() {
			return nbetudiant;
		}
		public void setNbetudiant(Integer nbetudiant) {
			this.nbetudiant = nbetudiant;
		}
		public Integer getNbprof() {
			return nbprof;
		}
		public void setNbprof(Integer nbprof) {
			this.nbprof = nbprof;
		}
		public Universite getUniversite() {
			return Universite;
		}
		public void setUniversite(Universite universite) {
			this.Universite = universite;
		}
	    
}
