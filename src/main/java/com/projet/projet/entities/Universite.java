package com.projet.projet.entities;


import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.projet.projet.entities.Departement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data @NoArgsConstructor @AllArgsConstructor
public class Universite  implements Serializable{
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long idUniversite;
	    private String nomUniversite;
	    private String description;
	    private String adresse;
	    private String photo ;
	    @JsonIgnore
	    @OneToMany(cascade = CascadeType.ALL,mappedBy = "universite")
	    private List<Departement> departments;
	    @OneToOne(cascade = CascadeType.ALL)
	    private DetailUniversite detailUniversite;
		public DetailUniversite getDetailUniversite() {
			return detailUniversite;
		}
		public String getPhoto() {
			return photo;
		}
		public void setPhoto(String photo) {
			this.photo = photo;
		}
		public void setDetailUniversite(DetailUniversite detailUniversite) {
			detailUniversite = detailUniversite;
		}
		public Long getIdUniversite() {
			return idUniversite;
		}
		public void setIdUniversite(Long idUniversite) {
			this.idUniversite = idUniversite;
		}
		public String getNomUniversite() {
			return nomUniversite;
		}
		public void setNomUniversite(String nomUniversite) {
			this.nomUniversite = nomUniversite;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getAdresse() {
			return adresse;
		}
		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}
		public List<Departement> getDepartments() {
			return departments;
		}
		public void setDepartments(List<Departement> departments) {
			this.departments = departments;
		}
	    

}

