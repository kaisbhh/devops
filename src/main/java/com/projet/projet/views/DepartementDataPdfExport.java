package com.projet.projet.views;

import java.awt.Color;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.projet.projet.entities.Etudiant;
public class DepartementDataPdfExport extends AbstractPdfView{

	 @Override
	    protected void buildPdfMetadata(Map<String, Object> model, Document document, HttpServletRequest request)
	    {
	       Font headerFont = new Font(Font.TIMES_ROMAN, 20, Font.BOLD, Color.magenta);
	       HeaderFooter header = new HeaderFooter(new Phrase("Les etudiants", headerFont), false);
	       header.setAlignment(Element.ALIGN_CENTER);
	       document.setHeader(header);

	       Font dateFont = new Font(Font.HELVETICA, 12, Font.NORMAL, Color.BLUE);

	       HeaderFooter footer = new HeaderFooter(new Phrase("Exporté le : "+new Date(), dateFont), true);
	       footer.setAlignment(Element.ALIGN_CENTER);
	       document.setFooter(footer);
	    }
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.addHeader("Content-Disposition", "attachment;filename=etudiants.pdf");

	       //read data from controller
	       List<Etudiant> list = (List<Etudiant>) model.get("list");

	       //create element
	       Font titleFont = new Font(Font.TIMES_ROMAN, 24, Font.BOLD, Color.blue);
	       //Paragraph title = new Paragraph("Les etudiants", titleFont);
	      // title.setAlignment(Element.ALIGN_CENTER);
	      // title.setSpacingBefore(20.0f);
	       //title.setSpacingAfter(25.0f);
	       //add to document
	       //document.add(title);

	       Font tableHead = new Font(Font.TIMES_ROMAN, 12, Font.BOLD, Color.blue);
	       PdfPTable table = new PdfPTable(5);// no.of columns
	       table.addCell(new Phrase("Nom",tableHead));
	       table.addCell(new Phrase("Prenom",tableHead));
	       table.addCell(new Phrase("Email",tableHead));
	       table.addCell(new Phrase("Departement",tableHead));
	       table.addCell(new Phrase("Universite",tableHead));

	       for(Etudiant etudiant : list ) {
	          table.addCell(etudiant.getNomE());
	          table.addCell(etudiant.getPrenomE());
	          table.addCell(etudiant.getEmail());
	          table.addCell(etudiant.getDepartement().getNomDepartement());
	          table.addCell(etudiant.getDepartement().getUniversite().getNomUniversite());
	       }
	       //add table data to document
	       document.add(table);
	    }
		
	

}
