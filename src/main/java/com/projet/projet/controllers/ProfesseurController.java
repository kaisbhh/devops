package com.projet.projet.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.projet.projet.entities.Professeur;
import com.projet.projet.services.IServiceDepartement;
import com.projet.projet.services.IServiceProfesseur;


@Controller
@RequestMapping("/professeur")
public class ProfesseurController {
	@Autowired
	IServiceProfesseur pr;
	@Autowired
	IServiceDepartement dp;
	@GetMapping("all")
	public String getAllProfesseur(Model m) {
		List<Professeur> liste = pr.getAllProfesseur();
		m.addAttribute("liste",liste);
		m.addAttribute("departement",dp.retrieveAllDepartement());
		return "listeprofesseur";
	}
	@GetMapping("/ajouterprofesseur")
	public String ajouterprofesseur(Model m) {
		m.addAttribute("departement",dp.retrieveAllDepartement());
		return "ajouterprofesseur";
	}
	@PostMapping("/saveprofesseur")
	public String sauvegarderprofesseur(@ModelAttribute Professeur p,@RequestParam("file") MultipartFile mf,Model m) throws IOException {
		Integer id =p.getIdProf();
		pr.addProfesseur(p,mf);
		if(id!=null) {
			return "redirect:/professeur/all";
		}else {
			m.addAttribute("message","Ajout avec succes");
			m.addAttribute("departement",dp.retrieveAllDepartement());
			return "ajouterprofesseur";
		}
		
	}
	@GetMapping("/supprprofesseur/{id}")
	public String suprimerprofesseur(@PathVariable Integer id) throws IOException {
		pr.deleteProfesseur(id);
		return "redirect:/professeur/all";
	}
	@GetMapping("/modifierprofesseur/{idp}")
	public String modifierprofesseur(Model m,@PathVariable("idp") Integer id) {
		m.addAttribute("departement",dp.retrieveAllDepartement());
		m.addAttribute("professeur",pr.retrieveProfesseur(id));
		return "ajouterprofesseur";
	}
	@GetMapping("/assignprofesseurtodepartement")
    public String assignprof(@RequestParam("professeur") Integer idP , @RequestParam("departement") Long idDep)throws IOException
    {
        pr.assignprofesseurtodepartement(idP,idDep);
        return "redirect:/professeur/all";
        
     
    }
	@GetMapping("/affecterprof")
	public String affecterprofesseur(Model m) throws IOException {
		m.addAttribute("departement",dp.retrieveAllDepartement());
		m.addAttribute("professeur", pr.getAllProfesseur());
		return "affecterprofesseur";
	}
	
}
