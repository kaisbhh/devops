package com.projet.projet.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.projet.projet.entities.Etudiant;
import com.projet.projet.services.IServiceDepartement;
import com.projet.projet.services.IserviceEtudiant;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@Controller
@RequestMapping("/etudiant")
public class EtudiantController {
	@Autowired
	IserviceEtudiant et;
	@Autowired
	IServiceDepartement dp;
	@GetMapping("all")
	public String getAllEtudiant(Model m) {
		List<Etudiant> liste = et.getAllEtudiant();
		m.addAttribute("liste",liste);
		m.addAttribute("departement",dp.retrieveAllDepartement());
		return "listeetudiant";
	}
	@GetMapping("/ajouteretudiant")
	public String ajouteretudiant(Model m) {
		m.addAttribute("departement",dp.retrieveAllDepartement());
		return "ajouteretudiant";
	}
	@PostMapping("/saveetudiant")
	public String sauvegarderetudiant(@ModelAttribute Etudiant e,@RequestParam("file") MultipartFile mf,Model m) throws IOException {
		Integer id =e.getIdEtudiant();
		et.addEtudiant(e,mf);
		if(id!=null) {
			return "redirect:/etudiant/all";
		}else {
			m.addAttribute("message","Ajout avec succes");
			m.addAttribute("departement",dp.retrieveAllDepartement());
			return "ajouteretudiant";
		}
		
	}
	@GetMapping("/etudiantpardepartement")
	public String getetudiantpardepartement(@RequestParam("departement") Long id,Model m) {
		m.addAttribute("departement",dp.retrieveAllDepartement());
		if(id==0) {
			m.addAttribute("liste", et.getAllEtudiant());
			return "redirect:/etudiant/all";
		}else {
			m.addAttribute("liste", dp.getetudiantpardepartement(id));
			m.addAttribute("etudiant",dp.getDepartement(id).getNomDepartement());
			System.out.println(dp.getDepartement(id).getNomDepartement());
			return "listeetudiant";
		}
		
	}
	@GetMapping("/suppretudiant/{id}")
	public String suprimeretudiant(@PathVariable Integer id) throws IOException {
		et.deleteEtudiant(id);
		return "redirect:/etudiant/all";
	}
	@GetMapping("/modifieretudiant/{idp}")
	public String modifieretudiant(Model m,@PathVariable("idp") Integer id) {
		m.addAttribute("departement",dp.retrieveAllDepartement());
		m.addAttribute("etudiant",et.retrieveEtudiant(id));
		return "editeretudiant";
	}
	@PutMapping("/saveedit")
	public String sauvegardeupdate(@RequestBody Etudiant e,Model m) throws IOException {
		Integer id =e.getIdEtudiant();
		et.updateEtudiant(e,id);
		
			m.addAttribute("message","edit avec succes");
			
			return "redirect:/etudiant/all";
		
	}
	
}
