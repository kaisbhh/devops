package com.projet.projet.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.projet.projet.entities.Departement;
import com.projet.projet.entities.Etudiant;
import com.projet.projet.services.IServiceChef;
import com.projet.projet.services.IServiceDepartement;
import com.projet.projet.services.IServiceUniversite;
import com.projet.projet.views.DepartementDataPdfExport;
@Controller
@RequestMapping("/departement")


public class DepartementController {
	@Autowired
	IServiceDepartement dp;
	@Autowired
	IServiceUniversite un;
	@Autowired
	IServiceChef c;
	@GetMapping("all")
	public String getAllUniversite(Model m) {
		List<Departement> liste = dp.retrieveAllDepartement();
		m.addAttribute("liste",liste);
		m.addAttribute("universite",un.getAllUniversite());
		return "listedepartement";
	}
	@GetMapping("/ajouterdepartement")
	public String ajouterdepartement(Model m) {
		m.addAttribute("universite",un.getAllUniversite());
		m.addAttribute("chef",c.getAllChefDepart());
		return "ajouterdepartement";
	}
	@PostMapping("/savedepartement")
	public String sauvegarderdepartement(@ModelAttribute Departement d,Model m) throws IOException {
		Long id =d.getIdDepartement();
		dp.addDepartement(d);
		if(id!=null) {
			return "redirect:/departement/all";
		}else {
			m.addAttribute("message","Ajout avec succes");
			m.addAttribute("universite",un.getAllUniversite());
			return "ajouterdepartement";
		}
		
	}
	@GetMapping("/departementparuniversite")
	public String getDepartementparuniversite(@RequestParam("universite") Long id,Model m) {
		m.addAttribute("universite",un.getAllUniversite());
		if(id==0) {
			m.addAttribute("liste", dp.retrieveAllDepartement());
			return "redirect:/departement/all";
		}else {
			m.addAttribute("liste", un.getDepartementbyuniversite(id));
			m.addAttribute("departement",un.getUniversite(id).getNomUniversite());
			System.out.println(un.getUniversite(id).getNomUniversite());
			return "listedepartement";
		}
		
	}
	@GetMapping("/supprdepartement/{id}")
	public String suprimerdepartement(@PathVariable Long id) throws IOException {
		dp.deleteDepartement(id);
		return "redirect:/departement/all";
	}
	@GetMapping("/modifierdepartement/{idp}")
	public String modifierdepartement(Model m,@PathVariable("idp") Long id) {
		m.addAttribute("universite",un.getAllUniversite());
		m.addAttribute("departement",dp.getDepartement(id));
		m.addAttribute("chef",c.getAllChefDepart());
		dp.deleteDepartement(id);
		return "ajouterdepartement";
	}
	@GetMapping("/detaildepartement/{id}")
	public String detaildepartement(@PathVariable Long id,Model m) throws IOException {
		m.addAttribute("departement",dp.getDepartement(id));
		return "detaildepartement";
	}
	
	@GetMapping("/pdf/{idp}")
	public ModelAndView exportToPdf(@PathVariable("idp") Long id) {
	    ModelAndView mav = new ModelAndView();
	    mav.setView(new DepartementDataPdfExport());
	  //read data from DB
	    List<Etudiant> list= dp.getetudiantpardepartement(id);
	  //send to pdfImpl class
	    mav.addObject("list", list);
	    return mav; 
	}
	
}
