package com.projet.projet.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.projet.projet.entities.Universite;
import com.projet.projet.services.IServiceDetail;
import com.projet.projet.services.IServiceUniversite;

@Controller
@RequestMapping("/universite")

public class UniversiteController {
	@Autowired
	IServiceUniversite un;
	@Autowired
	IServiceDetail det ; 
	
	@GetMapping("all")
	public String getAllUniversite(Model m) {
		List<Universite> liste = un.getAllUniversite();
		m.addAttribute("liste",liste);
		m.addAttribute("detail",det.retrieveAlldetail());
		m.addAttribute("detail", "Toutes les details");
		return "listeuniversite";
	}
	@GetMapping("/ajouteruniversite")
	public String ajouteruniversite(Model m) {
		m.addAttribute("detail",det.retrieveAlldetail());
		return "ajouteruniversite";
	}
	@PostMapping("/saveuniversite")
	public String sauvegarderuniversite(@ModelAttribute Universite u,@RequestParam("file") MultipartFile mf,Model m) throws IOException {
		Long id =u.getIdUniversite();
		un.addUniversite(u,mf);
		if(id!=null) {
			return "redirect:/universite/all";
		}else {
			m.addAttribute("message","Ajout avec succes");
			m.addAttribute("detail",det.retrieveAlldetail());
			return "ajouteruniversite";
		}
		
	}
	@GetMapping("/suppruniversite/{id}")
	public String suprimerProduit(@PathVariable Long id) throws IOException {
		un.deleteUniversite(id);
		return "redirect:/universite/all";
	}
	@GetMapping("/modifieruniversite/{idp}")
	public String modifierProduit(Model m,@PathVariable("idp") Long id) {
		m.addAttribute("detail",det.retrieveAlldetail());
		m.addAttribute("universite",un.getUniversite(id));
		return "ajouteruniversite";
	}
	@GetMapping("/universitename")
	public String getuniversiteparname(@RequestParam String mc ,Model m) {
		List<Universite> liste = un.getUniversiteByName(mc);
		m.addAttribute("liste",liste);
		return "listeuniversite";
	}
}
