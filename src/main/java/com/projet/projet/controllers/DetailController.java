package com.projet.projet.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.projet.projet.entities.DetailUniversite;
import com.projet.projet.services.IServiceDetail;
import com.projet.projet.services.IServiceUniversite;
@Controller
@RequestMapping("/detail")
public class DetailController {
	@Autowired
	IServiceUniversite un;
	@Autowired
	IServiceDetail det ;
	
	@GetMapping("all")
	public String getAllDetail(Model m) {
		List<DetailUniversite> liste = det.retrieveAlldetail();
		m.addAttribute("liste",liste);
		m.addAttribute("universite",un.getAllUniversite());
		return "listedetail";
	}
}
