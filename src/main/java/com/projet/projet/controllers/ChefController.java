package com.projet.projet.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.projet.projet.entities.ChefDepart;
import com.projet.projet.services.IServiceChef;
import com.projet.projet.services.IServiceDepartement;

@Controller
@RequestMapping("/chef")
public class ChefController {
	@Autowired
	IServiceChef C;
	@Autowired
	IServiceDepartement dp;
	@GetMapping("all")
	public String getAllchef(Model m) {
		List<ChefDepart> liste =C.getAllChefDepart();
		m.addAttribute("liste",liste);
		return "listechef";
	}
	@GetMapping("/ajouterchef")
	public String ajouterchef(Model m) {
		m.addAttribute("departement",dp.retrieveAllDepartement());
		return "ajouterchef";
	}
	@PostMapping("/savechef")
	public String sauvegarderchef(@ModelAttribute ChefDepart c,@RequestParam("file") MultipartFile mf,Model m) throws IOException {
		Integer id =c.getIdC();
		C.addChefDepart(c, mf);
		if(id!=null) {
			return "redirect:/chef/all";
		}else {
			m.addAttribute("message","Ajout avec succes");
			m.addAttribute("departement",dp.retrieveAllDepartement());
			return "ajouterchef";
		}
		
	}
	@GetMapping("/supprchef/{id}")
	public String suprimerchef(@PathVariable Integer id) throws IOException {
		C.deleteChefDepart(id);
		return "redirect:/chef/all";
	}
	@GetMapping("/modifierchef/{idp}")
	public String modifierchef(Model m,@PathVariable("idp") Integer id) {
		
		m.addAttribute("chef",C.retrieveChefDepart(id));
		return "ajouterchef";
	}

}
