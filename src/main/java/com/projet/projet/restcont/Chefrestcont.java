package com.projet.projet.restcont;

import java.io.IOException;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projet.projet.entities.ChefDepart;
import com.projet.projet.services.IServiceChef;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/apichef")
@CrossOrigin(origins = {"http://localhost:4200"})
public class Chefrestcont {
	IServiceChef C;

	public Chefrestcont(IServiceChef c) {
		super();
		C = c;
	}
	@GetMapping("/getallchef")
			public List<ChefDepart> getAllchef(Model m) {
			return C.getAllChefDepart();
			
		}
	@PostMapping("/addchef/")
	public void  addchef(@RequestParam("chefDepart") String c , @RequestParam("file") MultipartFile mf) throws IOException, JsonProcessingException
	{
		ChefDepart d = new ObjectMapper().readValue(c,ChefDepart.class);
	    C.addChefDepart(d, mf);
	}
	@DeleteMapping("/deletechef/{id}")
	public  void deletechef(@PathVariable("id") Integer id) throws IOException {

	    C.deleteChefDepart(id);
	}
	@PutMapping("/updatechef/{id}")
	public void updatechef(@PathVariable("id") Integer id, @RequestBody ChefDepart c){

	    c.setIdC(id);
	    C.updateChefDepart(c);
	}
	@GetMapping(path = "/getImage/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getImage(@PathVariable("id") Integer id) throws IOException {
		return C.getimage(id) ; 
	}
	@GetMapping("/getchef/{id}")
	public ChefDepart Getchefbyid(@PathVariable("id") Integer id){

	    return  C.retrieveChefDepart(id);
	} 
	
}
