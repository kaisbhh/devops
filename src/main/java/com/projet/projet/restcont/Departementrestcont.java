package com.projet.projet.restcont;

import java.io.IOException;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.projet.projet.entities.Departement;
import com.projet.projet.entities.Etudiant;
import com.projet.projet.services.IServiceDepartement;
import com.projet.projet.services.IServiceUniversite;
import com.projet.projet.views.DepartementDataPdfExport;

import lombok.AllArgsConstructor;
@RestController
@AllArgsConstructor
@RequestMapping("/apidepartement")
@CrossOrigin(origins = {"http://localhost:4200"})
public class Departementrestcont {
	IServiceDepartement dp;
	IServiceUniversite un;

	public Departementrestcont(IServiceDepartement dp, IServiceUniversite un) {
		super();
		this.dp = dp;
		this.un = un;
	}
	
	@GetMapping("/getallDepartement")
	public List<Departement> Getdepartement(){

	    return  dp.retrieveAllDepartement();
	}
	@PostMapping("/adddepartement/")
	public  ResponseEntity<Departement> addUniv(@RequestBody Departement departement ) throws IOException, JsonProcessingException
	{
		 return new ResponseEntity<>(dp.addDepartement(departement),HttpStatus.CREATED);	    
	}
	@DeleteMapping("/deletedepartement/{id}")
	public  void deleteDepartement(@PathVariable("id") Long id) throws IOException {

	    dp.deleteDepartement(id);
	}
	@PutMapping("/updatedepartement/{id}")
	public void updatedepartement(@PathVariable("id") Long id, @RequestBody Departement D){

	    
	    dp.updateDepartement(D,id);
	}
	@GetMapping("/departementparuniversite/{id}")
	public List<Departement> getdepartementparuniversite(@PathVariable("id") Long id) {
		return un.getDepartementbyuniversite(id) ; 
		
	}
	@GetMapping("/getdepartement/{id}")
	public Departement Getdpbyid(@PathVariable("id") Long id){

	    return  dp.getDepartement(id);
	} 
	
	@GetMapping("/pdf/{idp}")
	public ModelAndView exportToPdf(@PathVariable("idp") Long id) {
	    ModelAndView mav = new ModelAndView();
	    mav.setView(new DepartementDataPdfExport());
	  //read data from DB
	    List<Etudiant> list= dp.getetudiantpardepartement(id);
	  //send to pdfImpl class
	    mav.addObject("list", list);
	    return mav; 
	}
}
