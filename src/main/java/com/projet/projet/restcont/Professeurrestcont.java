package com.projet.projet.restcont;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projet.projet.entities.Professeur;
import com.projet.projet.services.IServiceProfesseur;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
@RestController
@AllArgsConstructor
@RequestMapping("/apiprofesseur")
@CrossOrigin(origins = {"http://localhost:4200"})
public class Professeurrestcont {
	IServiceProfesseur pr;

	public Professeurrestcont(IServiceProfesseur pr) {
		
		this.pr = pr;
	}
	@GetMapping("/getallprofesseur")
	public List<Professeur> Getprofesseur(){

	    return  pr.getAllProfesseur();
	}
	@PostMapping("/addprofesseur/")
	public void  addprofesseur(@RequestParam("professeur") String P,@RequestParam("file") MultipartFile mf) throws IOException, JsonProcessingException
	{
		Professeur p = new ObjectMapper().readValue(P,Professeur.class);
	    pr.addProfesseur(p, mf);
	}
	@DeleteMapping("/deleteprofesseur/{id}")
	public  void deleteprofesseur(@PathVariable("id") Integer id) throws IOException {

	    pr.deleteProfesseur(id);
	}
	@PutMapping("/updateprofesseur/{id}")
	public void updateprofesseur(@PathVariable("id") Integer id, @RequestBody Professeur p){

	    
	    pr.updateProfesseur(p,id);
	}
	@PutMapping("/assignprofesseurtodepartement/{idP}/{idD}")
    public void assignprof(@PathVariable("idP") Integer idP , @PathVariable("idD")  Long idDep)
    {
        pr.assignprofesseurtodepartement(idP,idDep);
       
    }
	@GetMapping(path = "/getImage/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getImage(@PathVariable("id") int id) throws IOException {
		return pr.getimage(id) ; 
	}
	@GetMapping("/getprofesseur/{id}")
	public Professeur Getpbyid(@PathVariable("id") Integer id){

	    return  pr.retrieveProfesseur(id);
	} 
}
