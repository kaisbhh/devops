package com.projet.projet.restcont;


import java.io.IOException;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projet.projet.entities.Etudiant;
import com.projet.projet.services.IServiceDepartement;
import com.projet.projet.services.IserviceEtudiant;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/apietudiant")
@CrossOrigin(origins = {"http://localhost:4200"})
public class Etudiantrestcont {
	
	IserviceEtudiant et;
	IServiceDepartement dp;

	public Etudiantrestcont(IserviceEtudiant et, IServiceDepartement dp) {
		super();
		this.et = et;
		this.dp = dp;
	}
	
	@GetMapping("/getalletudiant")
	public List<Etudiant> Getetudiant(){

	    return  et.getAllEtudiant();
	}
	@PostMapping("/addEtudiant/")
	public void  addetudiant(@RequestParam("etudiant") String E , @RequestParam("file") MultipartFile mf ) throws IOException, JsonProcessingException
	{
		Etudiant e = new ObjectMapper().readValue(E,Etudiant.class);
	    et.addEtudiant(e,mf);
	}
	@DeleteMapping("/deleteetudiant/{idet}")
	public  void deleteetudiant(@PathVariable("idet") Integer id) throws IOException {

	    et.deleteEtudiant(id);
	}
	@PutMapping("/updateetudiant/{idet}")
	public void updateetudiant(@PathVariable("idet") Integer id, @RequestBody Etudiant E){

	    et.updateEtudiant(E,id);
	}
	@GetMapping(path = "/getImage/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getImage(@PathVariable("id") int id) throws IOException {
		return et.getimage(id) ; 
	}
	@GetMapping("/etudiantpardepartement/{id}")
	public List<Etudiant> getetudiantpardepartement(@PathVariable("id") Long id) {
		return dp.getetudiantpardepartement(id) ;
		
	}
	
	@GetMapping("/getetudiant/{id}")
	public Etudiant Getebyid(@PathVariable("id") Integer id){

	    return  et.retrieveEtudiant(id);
	} 
}
