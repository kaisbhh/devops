package com.projet.projet.restcont;

import java.io.IOException;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projet.projet.entities.Universite;
import com.projet.projet.services.IServiceUniversite;

import lombok.AllArgsConstructor;
@RestController
@AllArgsConstructor
@RequestMapping("/apiuniversite")
@CrossOrigin(origins = {"http://localhost:4200"})
public class Universiterestcont {
	IServiceUniversite universiteService;
	public Universiterestcont(IServiceUniversite universiteService) {
	this.universiteService = universiteService;
}

@GetMapping("/getallUniversite")
public List<Universite> GetUni(){

    return  universiteService.getAllUniversite();
}


@PostMapping("/addUniversite/")
public  void addUniv(@RequestParam("universite") String U , @RequestParam("file") MultipartFile mf ) throws IOException, JsonProcessingException
     {
	Universite u = new ObjectMapper().readValue(U,Universite.class);
	
	universiteService.addUniversite(u, mf); }

@PutMapping("/updateUniversite/{idUni}")
public void updateUni(@PathVariable("idUni") Long id, @RequestBody Universite E){

    
    universiteService.updateUniversite(E,id);
}


@DeleteMapping("/deleteUniversite/{idUni}")
public  void deleteUni(@PathVariable("idUni") Long id) throws IOException {

    universiteService.deleteUniversite(id);
}
@GetMapping(path = "/getImage/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
public byte[] getImage(@PathVariable("id") Long id) throws IOException {
	return universiteService.getimage(id) ; 
}
@GetMapping("/universitename")
public List<Universite> getuniversiteparname(@RequestParam String mc) {
	
	return  universiteService.getUniversiteByName(mc);
}
@GetMapping("/getallUniversite/{id}")
public Universite GetUnibyid(@PathVariable("id") Long id){

    return  universiteService.getUniversite(id);
}

}
