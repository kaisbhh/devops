<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste Etudiant</title>
<link
href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
crossorigin="anonymous">
</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>

<
<br>
<form action="/etudiant/etudiantpardepartement" method="get">
		<div class="mb-3">
			<select class="form-select" name="departement" onchange="submit()" >
				<option value="0">Toutes les Departements</option>
				<c:forEach items="${departement}" var="d">
					<option value="${d.idDepartement}">${d.nomDepartement}</option>
				</c:forEach>
			</select>
		</div>
	</form>
<table class="table table-hover">
<thead class="thead-dark">
		<tr>
			
			<th class="table-primary" >Photo</th>
			<th class="table-primary">Prenom</th>
			<th class="table-primary">Nom</th>
			<th class="table-primary">genre</th>
			<th class="table-primary">email</th>
			<th class="table-primary">Departement</th>
			<th class="table-primary">Universite</th>
			
		</tr>
		</thead>
		<c:forEach items="${liste}" var="etudiant">
			<tr>
			<td class="table-primary">
					<c:choose>
				<c:when test="${p.photo==''}">
						<img  width="50" height="50" src="../images/inconnu.jpg">
						</c:when>
						<c:when test="${p.photo!=''}">
						<img  width="50" height="50" src="../images/${etudiant.photo}">
						</c:when>
					</c:choose>
				</td>
				<td class="table-primary" >${etudiant.prenomE}</td>
				<td class="table-primary">${etudiant.nomE}</td>
				<td class="table-primary">${etudiant.genre}</td>
				<td class="table-primary" >${etudiant.email}</td>
				
				<td class="table-primary">${etudiant.departement.nomDepartement}</td>
				<td class="table-primary">${etudiant.departement.universite.nomUniversite}</td>
				<td class="table-primary">
					<a href="/etudiant/suppretudiant/${etudiant.idEtudiant}"
					class="btn btn-danger">
						<span class="fa fa-trash"> Supprimer</span>
					</a>
					<a href="/etudiant/modifieretudiant/${etudiant.idEtudiant}" class="btn btn-warning">
						<span class="fa fa-edit">modifier</span>
					</a>
				</td>
				
						</tr>
		</c:forEach>
	</table>
		<br>
		<a href="/etudiant/ajouteretudiant"
					class="btn btn-success">
						<span class="fa fa-trash"> Ajouter etudiant</span>
					</a>
					<br>
<br>
	
					<div>

</div><br/>
</body>
</html>