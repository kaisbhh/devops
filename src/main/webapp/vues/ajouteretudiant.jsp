<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ajouter Etudiant</title>

</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>
<br>
<br>
<c:if test="${message!=null}">
		<div class="alert alert-success" role="alert">
		<c:out value="${message}"></c:out>
		${etudiant=null}
	</div>
</c:if>
<div class=container>
		<div class="card m-5 p-2">
			<div class="card-header"><h2>Ajouter Etudiant</h2></div>
			<div class="card-body">
				<form action="/etudiant/saveetudiant" method=post enctype="multipart/form-data">
					<div class="mb-3 mt-3">
					    <label for="prenomE" class="form-label">Prenom:</label>
						<input type=text class="form-control" id="prenomE" name="prenomE" value="${etudiant.prenomE}">
					</div>
					<div class="mb-3">
						<label for="nomE" class="form-label">Nom:</label>
						<input type=text class="form-control" id="nomE" name="nomE" value="${etudiant.nomE}">
					</div>
						<div class="mb-3">
						<label for="email" class="form-label">E-mail:</label>
						<input type=email class="form-control" id="email" name="email" value="${etudiant.email}">
					</div>
					<div class="mb-3">
					<input type=hidden name=photo value="${etudiant.photo}">
						<label for="photo" class="form-label">Photos:</label>
						<input type="file" class="form-control" id="photo" name="file" accept="image/png,image/jpeg">
					</div>
					<div class="mb-3">
						<label for="genre" class="form-label">Genre</label>
									   <select value="${etudiant.genre}" name="genre" class="form-control">
      <option value="Homme">homme</option>
      <option value="Femme">femme</option>
    </select>
					</div>
					
					<div class="mb-3">
						<label for="Departement" class="form-label">Departement</label>
						<select name="Departement" >
							<option selected hidden>Choose here</option>
							<c:forEach items="${departement}" var="d">
								<option value=${d.idDepartement} <c:if test="${etudiant.departement.idDepartement==d.idDepartement}">selected="true"</c:if>>${d.nomDepartement} </option>
							</c:forEach>
						</select>
					</div>
					<input type=hidden name=id value="${etudiant.idEtudiant}">
					<button type="submit" class="btn btn-primary" >Save</button>
				</form>
			</div> 
		</div>
	</div>
</body>
</html>