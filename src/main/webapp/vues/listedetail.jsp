<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste Universites</title>
<link
href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
crossorigin="anonymous">
</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>
<br>
<br>
<table class="table table-hover">
		<tr>
			
			
			
			<th>Adresse</th>
			<th>nombre etudiants</th>
			<th>nombre professeurs</th>
			<th>universite</th>
		</tr>
		<c:forEach items="${liste}" var="u">
			<tr>
				
				
				
				<td>${u.Annee}</td>
				<td>${u.nbetudiant}</td>
				<td>${u.nbprof}</td>
				<td>${u.Universite.nomUniversite}</td>
					
						</tr>
		</c:forEach>
	</table>
		<br>
<br>
	<a href="/universite/ajouteruniversite"
					class="btn btn-danger">
						<span class="fa fa-trash"> Ajouter Universite</span>
					</a>
</body>
</html>