<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>editer Etudiant</title>

</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>
<br>
<br>

<div class=container>
		<div class="card m-5 p-2">
			<div class="card-header"><h2>editer Etudiant</h2></div>
			<div class="card-body">
				<form   enctype="multipart/form-data">
					<div class="mb-3 mt-3">
					    <label for="prenomE" class="form-label">Prenom:</label>
						<input type=text class="form-control" id="prenomE" name="prenomE" value="${etudiant.prenomE}">
					</div>
					<div class="mb-3">
						<label for="nomE" class="form-label">Nom:</label>
						<input type=text class="form-control" id="nomE" name="nomE" value="${etudiant.nomE}">
					</div>
						<div class="mb-3">
						<label for="email" class="form-label">E-mail:</label>
						<input type=email class="form-control" id="email" name="email" value="${etudiant.email}">
					</div>
					<div class="mb-3">
					<input type=hidden name=photo value="${etudiant.photo}">
						<label for="photo" class="form-label">Photos:</label>
						<input type="file" class="form-control" id="photo" name="file" accept="image/png,image/jpeg">
					</div>
					<div class="mb-3">
						<label for="genre" class="form-label">Genre</label>
						<input type=text class="form-control" id="genre" name="genre" value="${etudiant.genre}">
					</div>
					
					<div class="mb-3">
						<label for="Departement" class="form-label">Departement</label>
						<select name="Departement" >
							<option selected hidden>Choose here</option>
							<c:forEach items="${departement}" var="d">
								<option value=${d.idDepartement} <c:if test="${etudiant.departement.idDepartement==d.idDepartement}">selected="true"</c:if>>${d.nomDepartement} </option>
							</c:forEach>
						</select>
					</div>
					
					<button type="submit" action="/etudiant/saveedit" class="btn btn-primary" >Save</button>
				</form>
			</div> 
		</div>
	</div>
</body>
</html>