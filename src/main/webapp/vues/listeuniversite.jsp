<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste Universites</title>
<link
href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
crossorigin="anonymous">
</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>


<br>

<div class="navbar navbar-light bg-light">
<form class="form-inline" action="/universite/universitename" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" id="mc" name="mc" value="${mc}">
    <br>
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
  </div>
<table class="table table-hover">
		<tr>
			
			<th class="table-primary">Photo</th>
			<th class="table-primary">Nom</th>
			<th class="table-primary">Adresse</th>
			<th class="table-primary">Description</th>
			
		</tr>
		<c:forEach items="${liste}" var="u">
			<tr>
				
					<td class="table-primary">
					<c:choose>
				<c:when test="${u.photo==''}">
						<img  width="50" height="50" src="../images/inconnu.jpg">
						</c:when>
						<c:when test="${p.photo!=''}">
						<img  width="100" height="50" src="../images/${u.photo}">
						</c:when>
					</c:choose>
				</td>
				<td class="table-primary">${u.nomUniversite}</td>
				<td class="table-primary">${u.adresse}</td>
				<td class="table-primary">${u.description}</td>
				<td class="table-primary">${u.detailUniversite.fondateur}</td>
				
				<td class="table-primary">
				<select class="form-select" name="universite"  >
				<option value="0">Toutes les Departements</option>
				<c:forEach items="${u.departments}" var="d">
					<option >${d.nomDepartement}</option>
				</c:forEach>
			</select>
			</td>
					<td class="table-primary">
					<a href="/universite/suppruniversite/${u.idUniversite}"
					class="btn btn-danger">
						<span class="fa fa-trash"> Supprimer</span>
					</a>
					<a href="/universite/modifieruniversite/${u.idUniversite}" class="btn btn-warning">
						<span class="fa fa-edit">modifier</span>
					</a>
				</td>
						</tr>
		</c:forEach>
	</table>
		<br>
		<a href="/universite/ajouteruniversite"
					class="btn btn-success">
						<span class="fa fa-trash"> Ajouter Universite</span>
					</a>
					<br>
<br>
	
</body>
</html>