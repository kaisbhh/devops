<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste Professeurs</title>
<link
href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
crossorigin="anonymous">
</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>


					
					
<hr>
<table class="table table-hover">
		<tr>
			
			<th class="table-primary">Photo</th>
			<th class="table-primary">Prenom</th>
			<th class="table-primary">Nom</th>
			<th class="table-primary">E-mail</th>
			<th class="table-primary">genre</th>
			
			
			
		</tr>
		<c:forEach items="${liste}" var="professeur">
			<tr>
			<td class="table-primary">
					<c:choose>
				<c:when test="${p.photo==''}">
						<img  width="50" height="50" src="../images/inconnu.jpg">
						</c:when>
						<c:when test="${p.photo!=''}">
						<img  width="50" height="50" src="../images/${professeur.photo}">
						</c:when>
					</c:choose>
				</td>
				<td class="table-primary">${professeur.prenomP}</td>
				<td class="table-primary">${professeur.nomP}</td>
				<td class="table-primary">${professeur.emailP}</td>
				<td class="table-primary">${professeur.genre}</td>
			
				<td class="table-primary">
				<select class="form-select" name="professeur"  >
				<option value="0">Toutes les Departements</option>
				<c:forEach items="${professeur.departements}" var="d">
					<option >${d.nomDepartement}  / ${d.universite.nomUniversite}</option>
				</c:forEach>
			</select>
			</td>
					
				
				<td class="table-primary">
					<a href="/professeur/supprprofesseur/${professeur.idProf}"
					class="btn btn-danger">
						<span class="fa fa-trash"> Supprimer</span>
					</a>
					<a href="/professeur/modifierprofesseur/${professeur.idProf}" class="btn btn-warning">
						<span class="fa fa-edit">modifier</span>
					</a>
				</td>
			
						</tr>
		</c:forEach>
	</table>
		<br>
		<a href="/professeur/ajouterprofesseur"
					class="btn btn-success">
						<span class="fa fa-trash"> Ajouter Professeur</span>
					</a>
					<hr>
					<a href="/professeur/affecterprof" class="btn btn-warning">
						<span class="fa fa-edit">affecter un professeur a une departement</span>
					</a>
<br>
	
</body>
</html>