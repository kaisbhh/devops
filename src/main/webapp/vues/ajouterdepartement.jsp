<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ajouter Departement</title>

</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>
<br>
<br>
<c:if test="${message!=null}">
		<div class="alert alert-success" role="alert">
		<c:out value="${message}"></c:out>
		${departement=null}
	</div>
</c:if>
<div class=container>
		<div class="card m-5 p-2">
			<div class="card-header"><h2>Ajouter Departement</h2></div>
			<div class="card-body">
				<form action="/departement/savedepartement" method=post enctype="multipart/form-data">
					<div class="mb-3 mt-3">
						<label for="nomDepartement" class="form-label">Nom:</label>
						<input type=text class="form-control" id="nomDepartement" name="nomDepartement" value="${departement.nomDepartement}">
					</div>
					<div class="mb-3">
						<label for="code" class="form-label">code:</label>
						<input type=text class="form-control" id="code" name="code" value="${departement.code}">
					</div>
					<div class="mb-3">
						<label for="description" class="form-label">description:</label>
						<input type=text class="form-control" id="description" name="description" value="${departement.description}">
					</div>
					<div class="mb-3">
						<label for="Universite" class="form-label">Universite:</label>
						<select name="universite" >
							<option selected hidden>Choose here</option>
							<c:forEach items="${universite}" var="u">
								<option value=${u.idUniversite} <c:if test="${departement.universite.idUniversite==u.idUniversite}">selected="true"</c:if>>${u.nomUniversite} </option>
							</c:forEach>
						</select>
					</div>
						<div class="mb-3">
						<label for="ChefDepart" class="form-label">Chef:</label>
						<select name="chef" >
							<option selected hidden>Choose here</option>
							<c:forEach items="${chef}" var="c">
								<option value=${c.idC} <c:if test="${departement.chef==c}">selected="true"</c:if>>${c.nomC} </option>
							</c:forEach>
						</select>
					</div>
					<input type=hidden name=id value="${departement.idDepartement}">
					<button type="submit" class="btn btn-primary" >Save</button>
				</form>
			</div> 
		</div>
	</div>
</body>
</html>