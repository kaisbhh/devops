<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste Professeurs</title>
<link
href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
crossorigin="anonymous">
</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>


<br>
<table class="table table-hover">
		<tr>
			
			<th class="table-primary">Photo</th>
			<th class="table-primary" >Prenom</th>
			<th class="table-primary" >Nom</th>
			<th class="table-primary">E-mail</th>
			<th class="table-primary">genre</th>
			<th class="table-primary">Departement</th>
			
			
			
		</tr>
		<c:forEach items="${liste}" var="chef">
			<tr>
			<td class="table-primary">
					<c:choose>
				<c:when test="${chef.photo==''}">
						<img  width="50" height="50" src="../images/inconnu.jpg">
						</c:when>
						<c:when test="${chef.photo!=''}">
						<img  width="50" height="50" src="../images/${chef.photo}">
						</c:when>
					</c:choose>
				</td>
				<td class="table-primary">${chef.prenomC}</td>
				<td class="table-primary">${chef.nomC}</td>
				<td class="table-primary">${chef.emailC}</td>
				<td class="table-primary">${chef.genre}</td>
				<td class="table-primary">${chef.departement.nomDepartement}/${chef.departement.universite.nomUniversite}</td>
				
					
				
				<td class="table-primary">
					<a href="/chef/supprchef/${chef.idC}"
					class="btn btn-danger">
						<span class="fa fa-trash"> Supprimer</span>
					</a>
					<a href="/chef/modifierchef/${chef.idC}" class="btn btn-warning">
						<span class="fa fa-edit">modifier</span>
					</a>
				</td>
			
						</tr>
		</c:forEach>
	</table>
		<br>
		<a href="/chef/ajouterchef"
					class="btn btn-success">
						<span class="fa fa-trash"> Ajouter chef</span>
					</a>
					<br>
<br>
	
</body>
</html>