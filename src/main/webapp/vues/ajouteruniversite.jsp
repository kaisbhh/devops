<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ajouter Universite</title>
<link
href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
crossorigin="anonymous">
</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>
<br>
<br>
<c:if test="${message!=null}">
		<div class="alert alert-success" role="alert">
		<c:out value="${message}"></c:out>
		${universite=null}
	</div>
</c:if>
<div class=container>
		<div class="card m-5 p-2">
			<div class="card-header"><h2>Ajouter Universite</h2></div>
			<div class="card-body">
				<form action="/universite/saveuniversite" method=post enctype="multipart/form-data">
					<div class="mb-3 mt-3">
						<label for="nomUniversite" class="form-label">Nom:</label>
						<input type=text class="form-control" id="nomUniversite" name="nomUniversite" value="${universite.nomUniversite}">
					</div>
					<div class="mb-3">
						<label for="adresse" class="form-label">Adresse:</label>
						<input type=text class="form-control" id="adresse" name="adresse" value="${universite.adresse}">
					</div>
					<div class="mb-3">
						<label for="description" class="form-label">description:</label>
						<input type=text class="form-control" id="description" name="description" value="${universite.description}">
					</div>
					<div class="mb-3">
					<input type=hidden name=photo value="${universite.photo}">
						<label for="photo" class="form-label">Photos:</label>
						<input type="file" class="form-control" id="photo" name="file" accept="image/png,image/jpeg">
					</div>
					
					<input type=hidden name=id value="${universite.idUniversite}">
					<button type="submit" class="btn btn-primary" >Save</button>
				</form>
			</div> 
		</div>
	</div>
</body>
</html>