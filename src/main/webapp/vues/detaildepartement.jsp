<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste Departements</title>
<link
href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
crossorigin="anonymous">
</head>
<body>
   <%@include file="menu.html" %>
<br>

<h1>Departement:</h1>${departement.nomDepartement} 
<hr>
<h2>Universite:</h2>${departement.universite.nomUniversite} 
<hr>
<h4>Chef departement:</h4>${departement.chef.nomC} ${departement.chef.prenomC}
<hr>
<h4>Les professeurs : </h4>
<table class="table table-hover">
		<tr>
			
			
			<th>Nom</th>
			<th>Prenom</th>
			<th>E-mail</th>
			
			
		</tr>
		<c:forEach items="${departement.professeurs}" var="p">
			<tr>
				
				
				<td>${p.nomP}</td>
				<td>${p.prenomP}</td>
				<td>${p.emailP}</td>
				
				
			
				
						</tr>
		</c:forEach>
	</table>
		<br>
<br>
<h4>Les etudiants : </h4>
<table class="table table-hover">
		<tr>
			
			
			<th>Nom</th>
			<th>Prenom</th>
			<th>E-mail</th>
			
			
		</tr>
		<c:forEach items="${departement.etudiants}" var="e">
			<tr>
				
				
				<td>${e.nomE}</td>
				<td>${e.prenomE}</td>
				<td>${e.email}</td>
				
				
			
				
						</tr>
		</c:forEach>
	</table>
	
</body>
</html>