<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste Departements</title>
<link
href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
crossorigin="anonymous">
</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>


					<br>
<br>
<form action="/departement/departementparuniversite" method="get">
		<div class="mb-3">
			<select class="form-select" name="universite" onchange="submit()" >
				<option value="0">Toutes les Universites</option>
				<c:forEach items="${universite}" var="u">
					<option value="${u.idUniversite}">${u.nomUniversite}</option>
				</c:forEach>
			</select>
		</div>
	</form>
<table class="table table-hover">
		<tr>
			
			
			<th class="table-primary">Nom</th>
			<th class="table-primary">Code</th>
			<th class="table-primary">Description</th>
			<th class="table-primary">Universite</th>
			<th class="table-primary">Chef departement</th>
			
			
		</tr>
		<c:forEach items="${liste}" var="departement">
			<tr>
				
				
				<td class="table-primary">${departement.nomDepartement}</td>
				<td class="table-primary">${departement.code}</td>
				<td class="table-primary">${departement.description}</td>
				<td class="table-primary">${departement.universite.nomUniversite}</td>
				<td class="table-primary">${departement.chef.nomC} ${departement.chef.prenomC}</td>
			
			<td class="table-primary">
				<a href="/departement/detaildepartement/${departement.idDepartement}"
					class="btn btn-danger">
						<span class="fa fa-trash"> Details</span>
					</a>
					</td>
				<td class="table-primary">
					<a href="/departement/supprdepartement/${departement.idDepartement}"
					class="btn btn-danger">
						<span class="fa fa-trash"> Supprimer</span>
					</a>
					<a href="/departement/modifierdepartement/${departement.idDepartement}" class="btn btn-warning">
						<span class="fa fa-edit">modifier</span>
					</a>
					<a href="/departement/pdf/${departement.idDepartement}" class="btn btn-success">Liste Des Etudaints 
 <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> 
				</td>
				
						</tr>
		</c:forEach>
	</table>
		<br>
		<a href="/departement/ajouterdepartement"
					class="btn btn-success">
						<span class="fa fa-trash"> Ajouter Departement</span>
					</a>
<br>

					<br>
					<br>
	
					
</body>
</html>