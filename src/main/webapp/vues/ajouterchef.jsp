<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ajouter chef</title>

</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>
<br>
<br>
<c:if test="${message!=null}">
		<div class="alert alert-success" role="alert">
		<c:out value="${message}"></c:out>
		${etudiant=null}
	</div>
</c:if>
<div class=container>
		<div class="card m-5 p-2">
			<div class="card-header"><h2>Ajouter chef</h2></div>
			<div class="card-body">
				<form action="/chef/savechef" method=post enctype="multipart/form-data">
					<div class="mb-3 mt-3">
					    <label for="prenomC" class="form-label">Prenom:</label>
						<input type=text class="form-control" id="prenomC" name="prenomC" value="${chef.prenomC}">
					</div>
					<div class="mb-3">
						<label for="nomC" class="form-label">Nom:</label>
						<input type=text class="form-control" id="nomC" name="nomC" value="${chef.nomC}">
					</div>
					<div class="mb-3">
						<label for="emailC" class="form-label">E-mail</label>
						<input type=email class="form-control" id="emailC" name="emailC" value="${chef.emailC}">
					</div>
					<div class="mb-3">
					<input type=hidden name=photo value="${chef.photo}">
						<label for="photo" class="form-label">Photos:</label>
						<input type="file" class="form-control" id="photo" name="file" accept="image/png,image/jpeg">
					</div>
					<div class="mb-3">
						<label for="genre" class="form-label">Genre</label>
						
					   <select value="${chef.genre}" name="genre" class="form-control">
      <option value="Homme">homme</option>
      <option value="Femme">femme</option>
    </select>
    </div>
					<input type=hidden name=id value="${chef.idC}">
					<button type="submit" class="btn btn-primary" >Save</button>
				</form>
			</div> 
		</div>
	</div>
</body>
</html>