<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ajouter Professeur</title>

</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>
<br>
<br>
<c:if test="${message!=null}">
		<div class="alert alert-success" role="alert">
		<c:out value="${message}"></c:out>
		${professeur=null}
	</div>
</c:if>
<div class=container>
		<div class="card m-5 p-2">
			<div class="card-header"><h2>Ajouter Professeur</h2></div>
			<div class="card-body">
				<form action="/professeur/saveprofesseur" method=post enctype="multipart/form-data">
					<div class="mb-3 mt-3">
					    <label for="prenomP" class="form-label">Prenom:</label>
						<input type=text class="form-control" id="prenomP" name="prenomP" value="${professeur.prenomP}">
					</div>
					<div class="mb-3">
						<label for="nomP" class="form-label">Nom:</label>
						<input type=text class="form-control" id="nomP" name="nomP" value="${professeur.nomP}">
					</div>
						<div class="mb-3">
						<label for="emailP" class="form-label">E-mail</label>
						<input type=email class="form-control" id="emailP" name="emailP" value="${professeur.emailP}">
					</div>
					<div class="mb-3">
					<input type=hidden name=photo value="${professeur.photo}">
						<label for="photo" class="form-label">Photos:</label>
						<input type="file" class="form-control" id="photo" name="file" accept="image/png,image/jpeg">
					</div>
					<div class="mb-3">
						<label for="genre" class="form-label">Genre</label>
									   <select value="${professeur.genre}" name="genre" class="form-control">
      <option value="Homme">homme</option>
      <option value="Femme">femme</option>
    </select>
					</div>
				
					<input type=hidden name=id value="${professeur.idProf}">
					<button type="submit" class="btn btn-primary" >Save</button>
				</form>
			</div> 
		</div>
	</div>
</body>
</html>