<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ajouter Departement</title>

</head>
<body>
   <%@include file="menu.html" %>
<br>
<br>
<br>
<br>
<c:if test="${message!=null}">
		<div class="alert alert-success" role="alert">
		<c:out value="${message}"></c:out>
		${departement=null}
	</div>
</c:if>
<div class=container>
		<div class="card m-5 p-2">
			<div class="card-header"><h2>Affecter professeur à une departement</h2></div>
			<div class="card-body">
				<form action="/professeur/assignprofesseurtodepartement/"  enctype="multipart/form-data">
						<div class="mb-3">
			<select class="form-select" name="professeur"  >
				<option value="0">Toutes les professeurs</option>
				<c:forEach items="${professeur}" var="p">
					<option value="${p.idProf}">${p.nomP}</option>
				</c:forEach>
			</select>
		</div>
					<div class="mb-3">
			<select class="form-select" name="departement"  >
				<option value="0">Toutes les Departements</option>
				<c:forEach items="${departement}" var="d">
					<option value="${d.idDepartement}">${d.nomDepartement}</option>
				</c:forEach>
			</select>
		</div>
	
					
					<button type="submit" class="btn btn-primary" >affecter</button>
				</form>
			</div> 
		</div>
	</div>
</body>
</html>